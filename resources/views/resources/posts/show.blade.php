@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">
                            {{ $post->title }}
                        </h3>

                        @can('update', $post)
                            <div class="pull-right">
                                <a class="btn btn-link" href="{{ route('posts.edit', $post) }}">edit</a>
                            </div>
                        @endcan

                        @can('delete', $post)
                            <div class="pull-right">
                                <form method="post" action="{{ route('posts.destroy', $post) }}">
                                    {!! csrf_field() !!}
                                    {!! method_field('delete') !!}
                                    <button type="submit" class="btn btn-link">
                                        delete
                                    </button>
                                </form>
                            </div>
                        @endcan

                        <div class="clearfix"></div>
                    </div>

                    <div class="panel-body">
                        {{ $post->body }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection