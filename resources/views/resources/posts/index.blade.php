@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">List of posts:</div>

                    <div class="panel-body">
                        @if(count($posts) > 0)
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Poster</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($posts as $post)
                                        <tr>
                                            <td>
                                                <a href="{{ route('posts.show', $post) }}">
                                                    {{ $post->id }}
                                                </a>
                                            </td>
                                            <td>{{ $post->title }}</td>
                                            <td>{{ $post->user->email }}</td>
                                            <td>{{ $post->created_at->format('d-m-Y') }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            There are no posts :(
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection