<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        Role::truncate();

        Role::create([
            'name' => 'user',
            'level' => 1
        ]);

        Role::create([
            'name' => 'editor',
            'level' => 2
        ]);

        Role::create([
            'name' => 'admin',
            'level' => 3,
            'is_admin' => true
        ]);

        Schema::enableForeignKeyConstraints();
    }
}
