<?php

namespace App\Policies;

use App\Post;
use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability, $model)
    {
        // Admin users are always allowed
        if($user->role->is_admin) {
            return true;
        }
    }

    /**
     * Check if the current User is authorized to update this Post
     *
     * @param User $user
     * @param Post $post
     *
     * @return bool
     */
    public function update(User $user, Post $post)
    {
        return (
            $user->id === $post->user_id
            and $user->role->level >= Role::level_editor
        );
    }

    /**
     * Check if the current User is authorized to create a new Post
     *
     * @param User $user
     *
     * @return bool
     */
    public function create(User $user)
    {
        return $user->role->level >= Role::level_editor;
    }

    /**
     * Check if the current User is authorized to delete this Post
     *
     * @param User $user
     * @param Post $post
     *
     * @return bool
     */
    public function delete(User $user, Post $post)
    {
        // Only admin users are allowed to delete posts
        return (
            $user->role->level >= Role::level_admin
        );
    }
}
