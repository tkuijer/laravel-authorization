@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Create a new Post
                    </div>

                    <div class="panel-body">
                        <form method="post" action="{{ route('posts.update', $post) }}" class="form-horizontal">
                            {!! method_field('put') !!}
                            {!! csrf_field() !!}

                            <div class="form-group">
                                <label for="title" class="col-sm-2 control-label">Title:</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" name="title" id="title" placeholder="Title" value="{{ $post->title }}" />
                                    {{ $errors->first('title', '') }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="body" class="col-sm-2 control-label">Body:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="body" id="body" cols="60" rows="4">{{ $post->body }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-default">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
