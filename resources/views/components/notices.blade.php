@if(Session::has('success'))
    @push('scripts')
        <script type="text/javascript">
            (function(){
                swal({
                    title: 'success',
                    text: '{{ Session::get('success') }}',
                    timer: 2500,
                    type: 'success'
                });
            })();
        </script>
    @endpush
@endif