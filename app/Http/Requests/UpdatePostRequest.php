<?php

namespace App\Http\Requests;

use Gate;
use App\Post;

class UpdatePostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $id_post = (int)$this->route('id_post');

        return Gate::allows('update', Post::findOrFail($id_post));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'body' => 'required'
        ];
    }
}
