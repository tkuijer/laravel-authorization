<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    const level_user = 1;
    const level_editor = 2;
    const level_admin = 3;

    protected $fillable = [
        'name',
        'is_admin',
        'level'
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
